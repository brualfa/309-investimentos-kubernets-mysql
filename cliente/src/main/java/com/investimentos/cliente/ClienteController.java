package com.investimentos.cliente;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class ClienteController {
	@Autowired
	private ClienteService clienteService;

	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public Cliente criar(@RequestBody Cliente cliente){
		return clienteService.cadastrar(cliente);
	}
	
	@GetMapping("/{cpf}")
	public Cliente buscar(@PathVariable String cpf) {
	  Optional<Cliente> optional = clienteService.buscar(cpf);
	  
	  if(optional.isPresent()) {
	    return optional.get();
	  }
	  
	  throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}
}
