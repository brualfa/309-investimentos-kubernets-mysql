package com.investimentos.aplicacao;

import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProdutoServiceIT {
  @Autowired
  private ProdutoService subject;
  
  @Test
  public void deveBuscarUmProduto() {
    Optional<Produto> optional = subject.consultar(5);
    
    assertTrue(optional.isPresent());
  }
}
