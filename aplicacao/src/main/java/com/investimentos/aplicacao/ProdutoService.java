package com.investimentos.aplicacao;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class ProdutoService {
  private RestTemplate restTemplate = new RestTemplate();
  private Logger logger = LoggerFactory.getLogger(ProdutoService.class);
  
  public Optional<Produto> consultar(int id) {
    try {
      Produto produto = restTemplate.getForObject("http://localhost:8080/" + id, Produto.class);
      return Optional.of(produto);
    } catch (HttpClientErrorException e) {
      logger.error(e.getMessage());
      return Optional.empty();
    }
  }
}
