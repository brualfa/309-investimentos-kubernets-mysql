package com.investimentos.aplicacao;

import org.springframework.data.repository.CrudRepository;

public interface AplicacaoRepository extends CrudRepository<Aplicacao, Integer> {

}
