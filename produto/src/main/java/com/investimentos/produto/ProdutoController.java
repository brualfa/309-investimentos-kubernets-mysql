package com.investimentos.produto;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class ProdutoController {
	@Autowired
	private ProdutoService produtoService;
	
	@GetMapping
	public Iterable<Produto> listar(){
		return produtoService.listar();
	}
	
	@GetMapping("/{id}")
	public Produto buscar(@PathVariable int id){
	   Optional<Produto> optional = produtoService.buscar(id);
	   
	   if(!optional.isPresent()) {
	     throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	   }
	   
	   return optional.get();
	}

}
